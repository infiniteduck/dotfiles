# Dotfiles

My dotfiles + [VSCodium](https://vscodium.com/) configs.
I use [dotdrop](https://gitlab.com/deadc0de6/dotdrop) to manage all my dotfiles.

